<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Assignment 1 introduces version control, and sets up the programs needed for later assignments.">
		<meta name="author" content="Jonathan Poteet">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment1</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> 
					<br>
					*Three Parts*
					<br>
					1. Distributed Version Control with Git and Bitbucket
					<br>
					2. Development Installations
					<br>
					3. Chapter questions, (Chs 1, 2)
				</p>

				<h4>Java Installation</h4>
				<img src="img/jdk.PNG" class="img-responsive center-block" alt="JDK Installation">

				<h4>Android Studio Installation</h4>
				<img src="img/androidstudio.PNG" class="img-responsive center-block" alt="Android Studio Installation">

				<h4>AMPPS Installation</h4>
				<img src="img/ampps.PNG" class="img-responsive center-block" alt="AMPPS Installation">
				
				<h4>AMPPS Installation Localhost</h4>
				<img src="img/amppslocalhost.PNG" class="img-responsive center-block" alt="AMPPS Installation">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
