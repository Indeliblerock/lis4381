> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381 Mobile Web Application Development

## Jonathan Poteet

### Assignment 1 Requirements:

*Three Parts*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter questions, (Chs 1, 2)

#### README.md file should include the following items:

* Screenshot of AMPPS installation
* Screenshot of running Java Hello
* Screenshot of running Android Studio - My First App
* Git commands w/ short descriptions
* Bitbucket repo links a) this assignment and b) the completed tutorials above (bitbucketstationlocations)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Creates a new repository
2. git status - Lists changes to the files in the staging area
3. git add - Adds files to the staging area
4. git commit - Commits files or changes to files to files in the local repository
5. git push - Sends changes from the local repository to the remote repository
6. git pull - Takes changed files from the remote repository, and brings them to the local repository
7. git clone - Creates a copy of the working repository


#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](/a1/img/ampps.PNG)

![AMPPS Installation localhost Screenshot](/a1/img/amppslocalhost.PNG)

*Screenshot of running java Hello*:

![Java Hello Screenshot](/a1/img/javahello.PNG)

![JDK Installation Screenshot](/a1/img/jdk.PNG)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](/a1/img/androidstudio.PNG)

![My First App Screenshot](/a1/img/myfirstapp.PNG)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Link](https://bitbucket.org/indeliblerock/bitbucketstationlocations/ "Bitbucket Station Locations")

