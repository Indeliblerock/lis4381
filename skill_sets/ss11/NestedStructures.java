import java.util.Scanner;

import java.util.*;
public class NestedStructures
{
    public static void displayProgram() {
    System.out.print("\nProgram searches user-entered integer w/in array of integers.\n"  
    + "Create an array with the following values: 3, 2, 4, 99, -1, -5, 3, 7 \n\n");
     }


    public static void getArray() 
    {
       
        Scanner input = new Scanner(System.in);
        int[] arrayNum = new int[] {3, 2, 4, 99, -1, -5, 3, 7};
        int i = 0;
        int len = arrayNum.length;
        int num = 0;
        System.out.println("Array length: " + len);
        System.out.print("Enter search value: ");
        num = input.nextInt();
        System.out.println("");
        while (i < arrayNum.length)
        {
            if ((num) == arrayNum[i])
            {
                System.out.println(num + " found at index " + (i));
                i++;
            }
            else
            {
                System.out.println(num + " not found at index " + (i));
                i++;
            }
        }
        input.close();
    }

    public static void main(String[] args) 
    {
    displayProgram();
    getArray();

    }
}
