import java.util.Scanner;

public class DecisionStructures 
{
    public static void main(String[] args)
   {
    System.out.print("\nProgram evaluates user entered characters. \n" 
    + "Use following characters: W or w, C or c, H or h, N or n.\n"
    +"Use following decision structures: if... else, and switch\n\n");
    
    Scanner input = new Scanner(System.in);
    char choice = ' ';
    System.out.println("Phone types: W or w (Work), C or c (Cell), H or h (home), N or n (None) ");
    System.out.print("Enter Phone Type: ");
    String choiceStr = input.next().toLowerCase();
    choice = choiceStr.charAt(0);
    
    System.out.println("\nIf...else Statement");
        if (choice == 'w')
        {
            System.out.println("Phone Type: Work");
        }
        else if (choice == 'c')
        {
            System.out.println("Phone Type: Cell");
        }
        else if (choice == 'h')
        {
            System.out.println("Phone Type: Home");
        }
        else if (choice == 'n')
        {
            System.out.println("Phone Type: None");
        }
        else
        {
            System.out.println("That was not an option.");
        }

        System.out.println("\nSwitch Statement");

        switch(choice)
        {
        case 'w' : System.out.println("Phone Type: Work\n"); break;
        case 'c' : System.out.println("Phone Type: Cell\n"); break;
        case 'h' : System.out.println("Phone Type: Home\n"); break;
        case 'n' : System.out.println("Phone Type: None\n"); break;
        default : System.out.println("That was not an option.\n");
        }
    }
    
}