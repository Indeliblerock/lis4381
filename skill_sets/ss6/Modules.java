import java.util.Scanner;
public class Modules
{
    
    



    public static void displayProgram() {
    System.out.print("\nProgram prompts user for first name and age, then prints results\n" 
        + "Create three methods from the following requirements:\n"
        + "1) displayProgram(): void method that displays program requirements\n"
        + "2) myVoidMethod():\n"
        + "\ta. Accepts two arguments: String and Int\n"
        + "\tb. Print's Users First Name and Age\n"
        + "3) myValueReturningMethod(): \n"
        + "\ta. Accepts two arguments: String and Int\n"
        + "\tb. Returns string containing First Name and Age\n");
     }



    static void myVoidMethod(int age, String fname) {
      
        System.out.println("\nVoid method call: " + fname + " is " + age);
    }



    static String myValueReturningMethod(int age, String fname) 
    {
     String a = Integer.toString(age);
     String output = ("Value-returning method call: " + fname +  " is " + a);
        return output;
           
        
    }

    public static void main(String[] args) 
    {
    displayProgram();
    
    Scanner input = new Scanner(System.in);
    System.out.print("Enter First Name: ");
    String fname = input.nextLine();
    System.out.print("Enter Age: ");
    int age = input.nextInt();

    myVoidMethod(age, fname);
    System.out.print(myValueReturningMethod(age, fname));
    }
}
