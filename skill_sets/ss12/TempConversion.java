import java.util.Scanner;
public class TempConversion
{
    public static void displayProgram() {
    System.out.println("Temperature Conversion Program");
    System.out.print("\nProgram converts user-entered temperatures into Fahrenheit or Celsius scales.\n"  
    + "Program contunues to prompt for user entry until no longer requested.\n"
    +"Note: upper or lower case letters permitted. Though, incorrect entries are not permitted.\n"
    +"Note: Program does not validate numeric data (optional requirement).\n\n");
     }


    public static void getTemp() 
    {
       
        Scanner input = new Scanner(System.in);
        //boolean entry = false;
        
        String tempType = "";
        double conversion = 0;
        int ftemp = 0, ctemp = 0;
        String goAgain = "";
    do
    {
        //do
        //{
            // this prompts the user for a type
            System.out.print("\nFahrenheit to Celsius? Type 'f', or Celsius to Fahrenheit? Type 'c': ");
            tempType = input.next().toLowerCase();
            
            switch (tempType){
            //in the case fahrenheit
            case "f":  
            System.out.print("Enter temperature in Fahrenheit: ");
            ftemp = input.nextInt();
            conversion = (ftemp - 32) * 5 / 9;
            System.out.printf("Temperature in Celsius = " + conversion);
            //entry = true;    
            break;
            //in the case celsius 
            case "c": System.out.print("Enter temperature in Celsius: ");
            ctemp = input.nextInt();
            conversion = (ctemp * 9 / 5) + 32;
            System.out.printf("Temperature in Fahrenheit = " + conversion);
            //entry = true;    
            break;
            //if it is a problem
            default: System.out.println("Incorrect entry. Please try again.\n");
            tempType = "";
            }


        //} while (entry == false);
        //entry = false;
        System.out.print("\nDo you want to convert another temperature (y or n)? ");
        goAgain = input.next().toLowerCase();

    } while (goAgain.equals("y"));
        input.close();
        System.out.println("\nThank you for using our Temperature Conversion Program!");
    }

    public static void main(String[] args) 
    {
    displayProgram();
    getTemp();

    }
}
