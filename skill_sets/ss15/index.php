<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Skillset 15">
	<meta name="author" content="Jonathan Poteet">
	<link rel="icon" href="favicon.ico">

	<title>LIS4381 - Skillset 15</title>
		<?php include_once("../../css/include_css.php"); ?>
</head>
<body>

	<?php include_once("../../global/nav.php"); ?>

	<div class="container">
		<div class="starter-template">
					
					<div class="page-header">
						<?php include_once("global/header.php"); ?>	
					</div>

					<h2>File Data</h2>

						<form id="newForm" method="post" class="form-horizontal" action="process.php">
								<div class="form-group">
										<label class="col-sm-4 control-label">Comment:</label>
										<div class="col-sm-4">
												<textarea class="form-control" name="textinput" rows="5" cols="40"></textarea>
										</div>
								</div>
								

								<div class="form-group">
									<div class="col-sm-6 col-sm-offset-3">
									<button type="submit" class="btn btn-primary" name="submit" value="submit">Submit</button>
										</div>
								</div>
						
								
						
						</form>
						

		

			<?php include_once "global/footer.php"; ?>
			
		</div> <!-- end starter-template -->
 </div> <!-- end container -->

	
	<!-- Bootstrap JavaScript
	================================================== -->
	<!-- Placed at end of document so pages load faster -->
			<?php include_once("../../js/include_js.php"); ?>

<script type="text/javascript">
 //See Regular Expressions: http://www.qcitr.com/usefullinks.htm#lesson7
 $(document).ready(function() {

	/*$('#newForm').formValidation({
			message: 'This value is not valid',
			icon: {
					valid: 'fa fa-check',
					invalid: 'fa fa-times',
					validating: 'fa fa-refresh'
			},
			fields: {
					textinput: {
							validators: {
									notEmpty: {
									 message: 'Text input required'
									},
																	
							},
					},

					
					
			}
	});*/
});
</script>

</body>
</html>
