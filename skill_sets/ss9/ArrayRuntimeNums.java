import java.util.Scanner;

import java.util.Arrays;
public class ArrayRuntimeNums
{
    public static void displayProgram() {
    System.out.print("\n1. Program creates array at runtime.\n"  
    + "2. Program displays array size\n"
    + "3. Program rounds sum and average of numbers to two decimal places\n"
    + "4. Numbers *must* be float data type, not double\n\n");
     }


    public static void getArray() 
    {
        float num;
        num = 0;
        Scanner input = new Scanner(System.in);
        int arraySize;
        //1st input
        System.out.print("Enter Array Size: ");
        while(!input.hasNextInt())
        {
                System.out.println("Not a valid int number");
                input.next();
                System.out.print("Please try again! Enter the Array Size: ");            
            
        }
        arraySize = input.nextInt();

        float[] arrayFloat = new float[arraySize];
        
        int i = 0;

        while (i != arraySize)
        {
            System.out.print("Enter value " + (i+1) + ": ");
            while(!input.hasNextFloat())
            {
                    System.out.println("Not a valid float number");
                    input.next();
                    System.out.print("Please try again! Enter value " + (i+1) +": ");            
            
            }
            num = input.nextFloat();
            arrayFloat[i] = num;
            //System.out.println(num);
            i++;
        }
         float sum = 0;
        for (int j = 0; j < arrayFloat.length; ++j)
        {
           
            sum = arrayFloat[j] + sum;
        }
        float average;
        average = sum / arrayFloat.length;
        String strSum = String.format("%.2f", sum);
        String strAverage = String.format("%.2f", average);
        System.out.println("The sum is: " + strSum);
        System.out.println("The average is: " + strAverage);
    }

    public static void main(String[] args) 
    {
    displayProgram();
    getArray();

    }
}
