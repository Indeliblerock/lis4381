public class ArraysAndLoops 
{
    public static void main(String[] args)
   {
    System.out.print("\nProgram loops through array of strings. \n" 
    + "Use following values: dog, cat, bird, fish, insect.\n"
    +"Use following loop structures: for, enhanced for, while do...while.\n\n"
    +"Note: Pretest loops: for, enhanced for, while. Postttest loop: do while. \n");
    String str[] = {"dog", "cat", "bird", "fish", "insect"};

    System.out.println("\nFor Loop:");
    for (int i = 0; i <= 4; i++)
    {
        System.out.println(str[i]);
    }
    
    System.out.println("\nEnhanced for Loop: ");
    for (String element : str)
    {
        System.out.println(element);
    }
    
    int k = 0;
    System.out.println("\nwhile Loop: ");
    while (k<=4)
    {
            System.out.println(str[k]);
            k++;    
    }
    
    int l =0;
    System.out.println("\ndo...while Loop:");
    do
    {
        System.out.println(str[l]);
    }
    while (++l <= 4);
    
   }
}