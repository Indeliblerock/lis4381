import java.util.Scanner;
import java.util.Random;

public class RandomArray 
{
    public static void main(String[] args)
   {
    System.out.print("\nProgram prompts user to enter desired number of psuedorandom-generated integers (min 1)\n" 
    + "Use following loop structures: for, enhanced for, while, do...while\n");
    Scanner input = new Scanner(System.in);
    Random r = new Random();
    System.out.print("Enter desired number of pseudorandom-generated integers (min 1): ");
    int arraySize = input.nextInt();
    int i;
    int myArray[] = new int[arraySize];
    System.out.println("\nFor Loop:");
    for (i = 0; i < myArray.length; i++)
   {
    System.out.println(r.nextInt());
   }

   System.out.println("\nEnhanced for Loop:");
    for (int x: myArray)
   {
    System.out.println(r.nextInt());
   }

    i = 0;
    System.out.println("\nWhile Loop:");
    while (i < myArray.length)
        {
            System.out.println(r.nextInt());
            i++;
        }

    i = 0;
    System.out.println("\nDo...While Loop:");
    do
    {
        System.out.println(r.nextInt());
        ++i;
    }
    while (i < myArray.length);    
}
    
}