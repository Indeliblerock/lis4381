import java.util.Scanner;
public class LargestThreeNumbers
{
    public static void displayProgram() {
    System.out.print("\nProgram evaluates largest of three integers.\n"  
    + "Note: Program checks for integers and non-numeric values \n\n");
     }


    public static void calculateLargest() 
    {
        int num1, num2, num3;
        num1 = 0;
        num2 = 0;
        num3 = 0;
        Scanner input = new Scanner(System.in);
        
        //1st input
        

        System.out.print("Enter the first number: ");
        while(!input.hasNextInt())
        {
                System.out.println("Not a valid integer.");
                input.next();
                System.out.print("Please try again! Enter the first number: ");            
            
        }
        num1 = input.nextInt();

        //2nd input
        System.out.print("Enter the second number:");
        while(!input.hasNextInt())
        {
                System.out.println("Not a valid integer.");
                input.next();
                System.out.print("Please try again! Enter the second number: ");            
            
        }
        num2 = input.nextInt();

        //3rd input
        System.out.print("Enter the third number:");
        while(!input.hasNextInt())
        {
            
                System.out.println("Not a valid integer.");
                input.next();
                System.out.print("Please try again! Enter the third number: ");            
            
        }
        num3 = input.nextInt();
        
        //sorting
        
        if ((num1 > num2) && (num1 > num3))
        {
            System.out.println("The first number is largest");
        }
        else if ((num2 > num1) && (num2 > num3))
        {
            System.out.println("The second number is largest");
        }
        else if ((num3 > num2) && (num3 > num1))
        {
            System.out.println("The third number is largest");
        }
        else if ((num3 == num2) && (num3 > num1))
        {
            System.out.println("2 and 3 are the largest.");
        
    }
    else if ((num1 == num2) && (num1 > num3))
        {
            System.out.println("1 and 2 are the largest.");
        
    }
    else if ((num3 == num2) && (num2 == num1))
        {
            System.out.println("All are equal.");
        }
    }

    public static void main(String[] args) 
    {
    displayProgram();
    calculateLargest();

    }
}
