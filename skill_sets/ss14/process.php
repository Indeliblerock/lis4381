<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Skillset 14">
	<meta name="author" content="Jonathan Poteet">
	<link rel="icon" href="favicon.ico">

	<title>LIS4381 - SkillSet 14</title>
		<?php include_once("../../css/include_css.php"); ?>
</head>
<body>

	<?php include_once("../../global/nav.php"); ?>

	<div class="container">
		<div class="starter-template">
					
					<div class="page-header">
						<?php include_once("global/header.php"); ?>	
					</div>

					
					<?php
					if(!empty($_POST))
					{
					$num1 = $_POST['num1']; 
					$num2 = $_POST['num2'];
					$process = $_POST['number'];
					
					// add if statement for that includes if(!empty($_POST)) statement
					
					
					if ($process === "addition")
					{
						echo "<h2>Addition</h2>";
						$calculation = $num1 + $num2;
						$operator = "+";
						echo "$num1 $operator $num2 = $calculation <br><br>";
					}
					elseif ($process === "subtraction")
					{
						echo "<h2>Subtraction</h2>";
						$calculation = $num1 - $num2;
						$operator = "-";
						echo "$num1 $operator $num2 = $calculation <br><br>";
					}
					elseif ($process === "multiplication")
					{
						echo "<h2>Multiplication</h2>";
						$calculation = $num1 * $num2;
						$operator = "*";
						echo "$num1 $operator $num2 = $calculation <br><br>";
					}
					elseif ($process === "division" && $num2 != 0)
					{
						echo "<h2>Division</h2>";
						$calculation = $num1 / $num2;
						$operator = "/";
						echo "$num1 $operator $num2 = $calculation <br><br>";
					}
					elseif ($process === "exponentiation")
					{
						echo "<h2>Exponentiation</h2>";
						$calculation = pow($num1, $num2);
						$operator = "**";
						echo "$num1 $operator $num2 = $calculation <br><br>";
					}
					else
					{
						echo "Cannot Divide By Zero! <br><br>";
					}
				}
				else
				{
					echo "Please fill in the required information! <br><br>";
				}
					
					
					
					?>





			<?php include_once("global/footer.php"); ?>
			
		</div> <!-- end starter-template -->
 </div> <!-- end container -->

	
	<!-- Bootstrap JavaScript
	================================================== -->
	<!-- Placed at end of document so pages load faster -->
			<?php include_once("../../js/include_js.php"); ?>


</body>
</html>
