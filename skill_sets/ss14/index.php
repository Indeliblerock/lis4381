<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Skillset 14">
	<meta name="author" content="Jonathan Poteet">
	<link rel="icon" href="favicon.ico">

	<title>LIS4381 - Assignment4</title>
		<?php include_once("../../css/include_css.php"); ?>
</head>
<body>

	<?php include_once("../../global/nav.php"); ?>

	<div class="container">
		<div class="starter-template">
					
					<div class="page-header">
						<?php include_once("global/header.php"); ?>	
					</div>

					<h2>Skillset 14</h2>

						<form id="newForm" method="post" class="form-horizontal" action="process.php">
								<div class="form-group">
										<label class="col-sm-4 control-label">Num1:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" name="num1" />
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-4 control-label">Num2:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" name="num2" />
										</div>
								</div>
								<div class="form-group">
									<input type="radio" name="number"  value="addition" checked> addition
									<input type="radio" name="number"  value="subtraction"> subtraction
									<input type="radio" name="number"  value="multiplication"> multiplication 
									<input type="radio" name="number"  value="division"> division
									<input type="radio" name="number"  value="exponentiation">exponentiation 
								<div>
								
								

								<div class="form-group">
									<div class="col-sm-6 col-sm-offset-3">
									<button type="submit" class="btn btn-primary" name="calculate" value="calculate">Calculate</button>
										</div>
								</div>
						
								
						
						</form>
						

		

			<?php include_once "global/footer.php"; ?>
			
		</div> <!-- end starter-template -->
 </div> <!-- end container -->

	
	<!-- Bootstrap JavaScript
	================================================== -->
	<!-- Placed at end of document so pages load faster -->
			<?php include_once("../../js/include_js.php"); ?>

<script type="text/javascript">
 //See Regular Expressions: http://www.qcitr.com/usefullinks.htm#lesson7
 $(document).ready(function() {

	$('#newForm').formValidation({
			message: 'This value is not valid',
			icon: {
					valid: 'fa fa-check',
					invalid: 'fa fa-times',
					validating: 'fa fa-refresh'
			},
			fields: {
					num1: {
							validators: {
									notEmpty: {
									 message: 'Number required'
									},
									stringLength: {
											min: 1,
											max: 10,
									 message: 'No more than 10 numbers'
									},
									regexp: {
											
										regexp: /^[0-9]*$/,
										message: 'Can only contain numbers'
									},									
							},
					},
					num2: {
							validators: {
									notEmpty: {
									 message: 'Number required'
									},
									stringLength: {
											min: 1,
											max: 10,
									 message: 'No more than 10 numbers'
									},
									regexp: {
											
										regexp: /^[0-9]*$/,
										message: 'Can only contain numbers'
									},									
							},
					},

					
					
			}
	});
});
</script>

</body>
</html>
