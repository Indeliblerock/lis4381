import java.util.Scanner;

import java.util.*;
public class ArrayListProgram
{
    public static void displayProgram() {
    System.out.print("\nProgram populates ArrayList of strings with user-entered animal type values.\n"  
    + "Examples: Polar bear, Guinea pig, dog, cat bird.\n"
    + "Program continues to collect user-entered values until user types 'n'.\n"
    + "Program displays ArrayList values after each iteration, as well as size.\n\n");
     }


    public static void getArray() 
    {
       
        Scanner input = new Scanner(System.in);
        char go = 'y';
        ArrayList<String> animals = new ArrayList<String>();
        String arrayAnimal = "";
        while (go == 'y')
        {
        System.out.print("\nEnter Animal Type: ");
        arrayAnimal = input.nextLine();
        animals.add(arrayAnimal);
        System.out.println("ArrayList elements: " + animals);
        System.out.println("ArrayList size: " + animals.size());

        System.out.print("Would you like to go again? (y or n): ");
        go = input.next().charAt(0);
        go = Character.toLowerCase(go);
        input.nextLine();
        }
        input.close();
    }

    public static void main(String[] args) 
    {
    displayProgram();
    getArray();

    }
}
