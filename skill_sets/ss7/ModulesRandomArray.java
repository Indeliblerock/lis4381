import java.util.Scanner;
import java.util.Random;
public class ModulesRandomArray
{
    public static void displayProgram() {
    System.out.print("\nProgram prompts user to enter desired number of psuedorandom-generated integers (min 1)\n" 
    + "Use following loop structures: for, enhanced for, while, do...while\n" 
    + "Note: Create and display using at least one value returning method, and one void method. \n\n");
     }



    public static void displayArrays(int num) 
    {
        Random r = new Random();
        int i = 0;

        int myArray[] = new int[num];
        System.out.println("\nfor Loop:");
        for(i=0; i< myArray.length; i++)
        {
            System.out.println(r.nextInt());
        }
        System.out.println("\nEnhanced for Loop:");
        for (int x: myArray)
        {
        System.out.println(r.nextInt());
        }

        i = 0;
        System.out.println("\nWhile Loop:");
        while (i < myArray.length)
        {
            System.out.println(r.nextInt());
            i++;
        }

        i = 0;
        System.out.println("\nDo...While Loop:");
        do
        {
            System.out.println(r.nextInt());
            ++i;
        }
        while (i < myArray.length);
    }



    public static int getArraySize() 
    {
        int arraySize;
        Scanner input = new Scanner(System.in);
        System.out.print("Enter desired number of pseudo-random integers: ");
        while (!input.hasNextInt())
        {
            System.out.println("Not a valid integer! \n");
            input.next();
            System.out.print("Please try again. Enter desired number of pseudo-random integers: ");
        }           
        arraySize = input.nextInt();
        return arraySize;
    }

    public static void main(String[] args) 
    {
    displayProgram();
    int arrayNum = getArraySize();
    displayArrays(arrayNum);
    }
}
