import java.util.Scanner;
//import java.lang.Math;

public class SphereVolume
{
    public static void displayProgram() {
    System.out.println("Sphere Volume Calculator");
    System.out.print("\nProgram converts sphere volume in liquid U.S. gallons from user-entered diameter value in inches,\n" 
    + "and rounds to two decimal places.\n"
    + "Must use Java's *built-in* PI and pow() capabilities. \n"
    + "Program checks for non-integers and non-numeric values. \n"
    + "Program continues to prompt for user entry until no longer requested, prompt accepts upper of lower case letters. \n");
     }


    public static void getVolume() 
    {
       
        Scanner input = new Scanner(System.in);
        //boolean entry = false;
        int diameter;
        double radius, gallons;
        String goAgain = "";
    do
    {
        System.out.print("\nPlease enter diameter in inches: ");
        while(!input.hasNextInt())
        {
                System.out.println("Not valid integer!\n");
                input.next();
                System.out.print("\nPlease try again! Enter diameter in inches: ");            
            
        }
        diameter = input.nextInt();
        radius = diameter / 2.0; 
        double volume = ((4.0/3.0) * Math.PI * Math.pow(radius, 3));
        gallons = volume/231;
        System.out.printf("Sphere volume: " + String.format("%,.2f", gallons) + " liquid U.S. gallons.\n");    
        

        
        System.out.print("\nDo you want to convert another sphere volume (y or n)? ");
        goAgain = input.next().toLowerCase();

    } while (goAgain.equals("y"));
        input.close();
        System.out.println("\nThank you for using our Sphere Volume Calculator!");
    }

    public static void main(String[] args) 
    {
    displayProgram();
    getVolume();

    }
}
