<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Project 1 has students create a personal business card application.">
		<meta name="author" content="Jonathan Poteet">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Project1</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> *5 Things*
				<br>
				1. Create an android app that acts as a business card.
				<br>
				2. Create a launcher icon image and display it in both activities (screens) 
				<br>
				3. Must add background color(s) to both activities
				<br>
				4. Must add border around image and button 
				<br>
				5. Must add text shadow (button)  
				</p>

				<h4>First User interface</h4>
				<img src="images/user1.PNG" class="img-responsive center-block" alt="User interface 1">

				<h4>Second User interface</h4>
				<img src="images/user2.PNG" class="img-responsive center-block" alt="User interface 2">



				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
