
# LIS4381 Mobile Web Application Development

## Jonathan Poteet

### Project 1 Requirements:

*5 Things*

1. Create an android app that acts as a business card.
2. Create a launcher icon image and display it in both activities (screens) 
3. Must add background color(s) to both activities
4. Must add border around image and button 
5. Must add text shadow (button) 

#### README.md file should include the following items:

* Course Title, Name, and Project Requirements
* Screenshot of first user interface
* Screenshot of second user interface



#### Assignment Screenshots:

*Screenshot of the first and second user interface*:

![1st interface screenshot](/p1/images/user1.PNG) ![2nd interface screenshot](/p1/images/user2.PNG)


