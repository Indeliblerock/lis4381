
# LIS 4381 Mobile Web Application Development

## Jonathan Poteet

### Assignment 3 Requirements:

*6 Things*

1. Create a ERD using MySQL
2. Enter at least 10 records in every table
3. Create the My Event Application in Android Studio
4. The My Event Application takes input and calculates the ticket price based on previously inputted values
5. Adds a border to the button and image.
6. Adds a textshadow to the button.

#### README.md file should include the following items:

* Screenshot of ERD
* Screenshot of first user interface
* Screenshot of second user interface
* links to: a3.mwb and a3.sql


#### Assignment 3 Screenshots:
*Screenshot of the ERD*:

![ERD Screenshot](/a3/images/erd.PNG)

*Screenshot of the Show Tables in the ERD*:

![Show Tables Screenshot](/a3/images/showtables.PNG)

*Screenshot of the consumer Table*:

![Customer Table Screenshot](/a3/images/customertable.PNG)
*Screenshot of the petstore Table*:

![Pet Store Table Screenshot](/a3/images/petstoretable.PNG)

*Screenshot of the pet Table*:

![Pet Table Screenshot](/a3/images/pettable.PNG)

*Screenshot of Android Studio - My Event - interfaces*:

![My Event 1st interface](/a3/images/userinterface.PNG)    ![My Event 2nd interface](/a3/images/userinterface1.PNG)


#### Assignment Links
*SQL ERD a3.mwb Link*
[a3.mwb](/a3/a3.mwb)

*SQL a3.sql Link*
[a3.sql](/a3/a3.sql)