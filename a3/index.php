<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Assignment 3 has us create a ERD using SQL and we created a mobile application using JavaScript.">
		<meta name="author" content="Jonathan Poteet">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment3</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> 
					<br>
					*4 Things* <br>

				1. Create a ERD using MySQL
				<br>
				2. Enter at least 10 records in every table
				<br>
				3. Create the My Event Application in Android Studio
				<br>
				4. The My Event Application takes input and calculates the ticket price based on previously inputted values
				<br>
				</p>

				<h4>ERD Screenshot</h4>
				<img src="images/erd.PNG" class="img-responsive center-block" alt="ERD Screenshot">

				<h4>First User interface</h4>
				<img src="images/userinterface.PNG" class="img-responsive center-block" alt="First user interface">

				<h4>Second User interface</h4>
				<img src="images/userinterface1.PNG" class="img-responsive center-block" alt="Second User interface">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
