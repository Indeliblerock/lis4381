<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Jonathan Poteet's online portfolio that illustrates skills acquired while working through various project requirements within LIS4381.">
	<meta name="author" content="Jonathan Poteet">
	<link rel="icon" href="favicon.ico">

	<title>My Online Portfolio</title>

		<?php include_once("css/include_css.php"); ?>	

<!-- Carousel styles -->
<style type="text/css">
h2
{
	margin: 0;     
	color: #666;
	padding-top: 50px;
	font-size: 52px;
	font-family: "trebuchet ms", sans-serif;    
}
.item
{
	background: #333;    
	text-align: center;
	height: 300px;
}
.carousel
{
  margin: 20px 0px 20px 0px;
  
}
.bs-example
{
  margin: 20px;
}
.img-fluid{
    max-width: 100%;
    height: auto;
}
</style>

</head>
<body>

	<?php include_once("global/nav_global.php"); ?>
	
	<div class="container">
		 <div class="starter-template">
						<div class="page-header">
							<?php include_once("global/header.php"); ?>	
						</div>

<!-- Start Bootstrap Carousel  -->
<div class="bs-example">
	<div
      id="myCarousel"
		class="carousel"
		data-interval="1000"
		data-pause="hover"
		data-wrap="true"
		data-keyboard="true"			
		data-ride="carousel">
		
    	<!-- Carousel indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>   
       <!-- Carousel items -->
        <div class="carousel-inner">

			<!-- -Note: you will need to modify the code to make it work with *both* text and images.  -->
          <div class="active item">
            <a href="http://localhost/repos/lis4381/a2/index.php">
                        <img src="images/landscape.JPG" alt="Slide 1" >
                        
                <div class="carousel-caption">
                <h2>Assignment 2</h2>
                  <h3>Android App</h3>
                  <p>Showcasing MyEvent app.</p>	
                    </div>
                    </a>
            </div>

            <div class="item">
            <a href="http://localhost/repos/lis4381/p1/index.php">
                <img src="images/bridge.JPG" alt="Slide 2">
                
                <div class="carousel-caption">
                <h2>Project 1</h2>
                  <h3>Business card app</h3>
                  <p>Showcasing Business card app.</p>
						 									
                </div>
                </a>
            </div>

            <div class="item">
            <a href="http://localhost/repos/lis4381/a4/index.php">
                <img src="images/country.JPG" alt="Slide 3" width=auto;>	
                
                <div class="carousel-caption">
                <h2>Assignment 4</h2>
                  <h3>Online Portfolio</h3>
                  <p>Showcasing data validation.</p>
						 								
                
                </div>
                </a>
            </div>

        </div>

        <!-- Carousel nav -->
        <a class="carousel-control left" href="#myCarousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <a class="carousel-control right" href="#myCarousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
    </div>
</div>
<!-- End Bootstrap Carousel  -->
						
<?php
include_once "global/footer.php";

?>

	</div> <!-- end starter-template -->
</div> <!-- end container -->

		<?php include_once("js/include_js.php"); ?>	
	
</body>
</html>
