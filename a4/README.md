
# LIS 4381 Mobile Web Application Development

## Jonathan Poteet

### Assignment 4 Requirements:

*4 things*

1. Created online portfolio using bootstrap.
2. Created carousel using bootstrap.
3. Used Jquery for data validation.
4. Uploaded a favicon.

#### README.md file should include the following items:

* Course Title, your name, assignment requirements
* Screenshots of the carousel, and data validation
* Link to local 4381 web app


#### Assignment Screenshots:

*Screenshot of the Carousel*

![Carousel Screenshot](images/carousel.PNG)

*Screenshot of validation 1*:

![Validation Screenshot 1 Screenshot](images/screenshotpass.PNG)

*Screenshot of validation 2*:

![Validation screenshot 2 Screenshot](images/screenshotpass1.PNG)


#### Localhost Link:

*Local link*
[Local Link](https://localhost/repos/lis4381 "Localhost link")
