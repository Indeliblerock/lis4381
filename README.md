
# LIS 4381 Mobile Web Application Development

## Jonathan Poteet

### LIS 4381 Requirements:

*Course Work Links*

1. [A1 README.md](https://bitbucket.org/Indeliblerock/lis4381/src/master/a1/README.md/ "Assignment 1")
    * Install AMPPS
    * Install JDK
    * Install Android Studio and create My First App
    * Provide screenshots of Installations
    * Complete Bitbucket Tutorials (bitbucketstationlocations and myteamquotes)
    * Provide git command descriptions 
2. [A2 README.md](https://bitbucket.org/Indeliblerock/lis4381/src/master/a2/README.md/ "Assignment 2")
    * Create Healthy Recipes Android App
    * Provide screenshots of completed app
3. [A3 README.md](https://bitbucket.org/Indeliblerock/lis4381/src/master/a3/README.md/ "Assignment 3")
    * Create ERD based upon business rules
    * Provide screenshot of completed ERD
    * Provide DB Resource links 
4. [A4 README.md](https://bitbucket.org/Indeliblerock/lis4381/src/master/a4/README.md/ "Assignment 4")
    * Create a online portfolio website.
    * Perform Data validation
    * Provide screenshots of the completed portfolio
    * Use Bootstrap to create carousel
5. [A5 README.md](https://bitbucket.org/Indeliblerock/lis4381/src/master/a5/README.md/ "Assignment 5")
    * Used PHP to pull data from a petstore database into my LIS4381 website.
    * Used server data validation to check the inputs of the data.
    * Displayed the data on the website.
6. [P1 README.md](https://bitbucket.org/Indeliblerock/lis4381/src/master/p1/README.md/ "Project 1")
    * Create a Business Card Android App
    * Provide screenshots of the completed app
7. [P2 README.md](https://bitbucket.org/Indeliblerock/lis4381/src/master/p2/README.md/ "Project 2")
    * Used PHP to add information to a database
    * Used PHP to edit information in a database
    * Used PHP to delete information from a database
    * Set up a RSS feed