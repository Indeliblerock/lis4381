
# LIS4381

## Jonathan Poteet

### Project 2 Requirements:

*3 Things*

1. Create a RSS feed page
2. Provide edit functionality to the edit button 
3. Provide delete functionality to the delete button

#### README.md file should include the following items:

* Course title, your name, assignment requirements for project 2
* Screenshots as per the examples shown in the requirements
* Localhost link to repository


#### Assignment Screenshots:

*RSS Feed*:

![RSS Feed](images/rssfeed.PNG)

*Edit*:

![Edit](images/edit.PNG)

*Error*:

![Error](images/error.PNG)

*Index*:

![index](images/index.PNG)

*Carousel*:

![carousel](images/carousel.PNG)


#### Tutorial Links:

*localhost link:*
[localhost Link](http://localhost/repos/lis4381/ "Localhost Link")
