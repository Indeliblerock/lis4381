# LIS 4381 Mobile Web Application Development

## Jonathan Poteet

### Assignment 2 Requirements:

*Three things*

1. Create a mobile recipe app using Android Studio
2. Provide Screenshots for first user interface.
3. Provide Screenshots for second user interface.

#### README.md file should include the following items:

* Course title, Your name, Assignment requirements
* Screenshot of running applications first user interface
* Screenshot of running applications second user interface


#### Assignment Screenshots:

*Running User Interface 1*:

![Running User Interface 1](/a2/images/userInterface1.PNG)

*Running User Interface 2*:

![Running User Interface 2](/a2/images/userInterface2.PNG)
