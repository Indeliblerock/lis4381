<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Assignment 2 has us creating a mobile app using android studio.">
		<meta name="author" content="Jonathan Poteet">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment2</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> 
					<br>
					*Three things*
					<br>
					1. Create a mobile recipe app using Android Studio
					<br>
					2. Provide Screenshots for first user interface.
					<br>
					3. Provide Screenshots for second user interface. 
				</p>

				<h4>First user interface</h4>
				<img src="images/userInterface1.PNG" class="img-responsive center-block" alt="User interface 1">

				<h4>Second user interface</h4>
				<img src="images/userInterface2.PNG" class="img-responsive center-block" alt="User interface 2">

				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
