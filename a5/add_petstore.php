<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Assignment 5 showcases skills working with bootstrap, PHP, and JavaScript.">
	<meta name="author" content="Jonathan Poteet">
	<link rel="icon" href="favicon.ico">

	<title>LIS4381 - Assignment5</title>
		<?php include_once("../css/include_css.php"); ?>
</head>
<body>

	<?php include_once("../global/nav.php"); ?>

	<div class="container">
		<div class="starter-template">
					
					<div class="page-header">
						<?php include_once("global/header.php"); ?>	
					</div>

					<h2>Pet Stores</h2>

						<form id="defaultForm" method="post" class="form-horizontal" action="add_petstore_process.php">
								<div class="form-group">
										<label class="col-sm-4 control-label">Name:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" name="name" placeholder="(max 30 characters)"/>
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-4 control-label">Street:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" name="street" placeholder="(max 30 characters)"/>
										</div>
								</div>
								<div class="form-group">
										<label class="col-sm-4 control-label">City:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" name="city" placeholder="(max 30 characters)"/>
										</div>
								</div>
								<div class="form-group">
										<label class="col-sm-4 control-label">State:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" name="state" placeholder="Example: FL"/>
										</div>
								</div>
								<div class="form-group">
										<label class="col-sm-4 control-label">Zip:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" name="zip" placeholder="(5 or 9 numbers, no dashes)"/>
										</div>
								</div>
								<div class="form-group">
										<label class="col-sm-4 control-label">Phone:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" name="phone" placeholder="10 digits only"/>
										</div>
								</div>
								<div class="form-group">
										<label class="col-sm-4 control-label">Email:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" name="email" placeholder="Example: you@gmail.com"/>
										</div>
								</div>
								<div class="form-group">
										<label class="col-sm-4 control-label">URL:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" name="url" placeholder="Example: https://www.example.com"/>
										</div>
								</div>
								<div class="form-group">
										<label class="col-sm-4 control-label">YTD Sales:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" name="ytdsales" placeholder="Example: 100.0"/>
										</div>
								</div>
								<div class="form-group">
										<label class="col-sm-4 control-label">Notes:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" name="notes"/>
										</div>
								</div>
								

								<div class="form-group">
									<div class="col-sm-6 col-sm-offset-3">
									<button type="submit" class="btn btn-primary" name="add" value="Add">Add</button>
										</div>
								</div>
						</form>

			<?php include_once "global/footer.php"; ?>
			
		</div> <!-- end starter-template -->
 </div> <!-- end container -->


<!-- remove if needed --------------------------------------------------------------------------------------------------------->
<!-- Placed at end of document so pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<!-- Turn off client-side validation, in order to test server-side validation. --> 
<!--<script type="text/javascript" src="../../js/formValidation/formValidation.min.js"></script> -->

<!-- Note the following bootstrap.min.js file is for form validation, different from the one above. -->
<script type="text/javascript" src="../../js/formValidation/bootstrap.min.js"></script>

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="../../js/ie10-viewport-bug-workaround.js"></script>
<!-- remove if needed --------------------------------------------------------------------------------------------------------->
	
	<!-- Bootstrap JavaScript
	================================================== -->
	<!-- Placed at end of document so pages load faster -->
	<?php //include_once("../js/include_js.php"); ?>

<script type="text/javascript">
 //See Regular Expressions: http://www.qcitr.com/usefullinks.htm#lesson7
 $(document).ready(function() {

	$('#defaultForm').formValidation({
			message: 'This value is not valid',
			icon: {
					valid: 'fa fa-check',
					invalid: 'fa fa-times',
					validating: 'fa fa-refresh'
			},
			fields: {
					name: {
							validators: {
									notEmpty: {
									 message: 'Name required'
									},
									stringLength: {
											min: 1,
											max: 30,
									 message: 'Name no more than 30 characters'
									},
									regexp: {
										//alphanumeric, hyphens, underscores, and spaces
										//regexp: /^[a-zA-Z0-9\-_\s]+$/,										
										//similar to: (though, \w supports other Unicode characters)
											regexp: /^[\w\-\s]+$/,
										message: 'Name can only contain letters, numbers, hyphens, and underscore'
									},									
							},
					},

					street: {
							validators: {
									notEmpty: {
											message: 'Street required'
									},
									stringLength: {
											min: 1,
											max: 30,
											message: 'Street no more than 30 characters'
									},
									regexp: {
										//street: only letters, numbers, commas, hyphens, space character, and periods
										regexp: /^[a-zA-Z0-9,\-\s\.]+$/,		
									message: 'Street can only contain letters, numbers, hyphens, commas, or periods'
									},									
							},
					},
					city: {
							validators: {
									notEmpty: {
									 message: 'City required'
									},
									stringLength: {
											min: 1,
											max: 30,
									 message: 'City no more than 30 characters'
									},
									regexp: {
										//alphanumeric, hyphens, underscores, and spaces
										//regexp: /^[a-zA-Z0-9\-_\s]+$/,										
										//similar to: (though, \w supports other Unicode characters)
											regexp: /^[a-zA-Z0-9,\s]+$/,
										message: 'City can only contain letters, number'
									},									
							},
					},
					state: {
							validators: {
									notEmpty: {
									 message: 'State required'
									},
									stringLength: {
											min: 2,
											max: 2,
									 message: 'State must be 2 characters.'
									},
									regexp: {
										//alphanumeric, hyphens, underscores, and spaces
										//regexp: /^[a-zA-Z0-9\-_\s]+$/,										
										//similar to: (though, \w supports other Unicode characters)
											regexp: /^[a-zA-Z]+$/,
										message: 'State can only include letters.'
									},									
							},
					},
					zip: {
							validators: {
									notEmpty: {
									 message: 'ZIP code required'
									},
									stringLength: {
											min: 5,
											max: 9,
									 message: 'Zip Code must be between 5 and 9 digits, inclusive'
									},
									regexp: {
										//alphanumeric, hyphens, underscores, and spaces
										//regexp: /^[a-zA-Z0-9\-_\s]+$/,										
										//similar to: (though, \w supports other Unicode characters)
											regexp: /^[0-9]+$/,
										message: 'Zip must only contain numbers'
									},									
							},
					},
					phone: {
							validators: {
									notEmpty: {
									 message: 'Phone number required'
									},
									stringLength: {
											min: 10,
											max: 10,
									 message: 'must be 10 digits, including area code'
									},
									regexp: {
										//alphanumeric, hyphens, underscores, and spaces
										//regexp: /^[a-zA-Z0-9\-_\s]+$/,										
										//similar to: (though, \w supports other Unicode characters)
											regexp: /^[0-9]+$/,
										message: 'must only contain numbers'
									},									
							},
					},
					email: {
							validators: {
									notEmpty: {
									 message: 'Email address required'
									},
									stringLength: {
											min: 1,
											max: 100,
									 message: 'Email no more than 100 characters'
									},
									regexp: {
										//alphanumeric, hyphens, underscores, and spaces
										//regexp: /^[a-zA-Z0-9\-_\s]+$/,										
										//similar to: (though, \w supports other Unicode characters)
											regexp: /^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/,
										message: 'Must include valid email.'
									},									
							},
					},
					url: {
							validators: {
									notEmpty: {
									 message: 'URL required'
									},
									stringLength: {
											min: 1,
											max: 100,
									 message: 'URL no more than 100 characters'
									},
									regexp: {
										//alphanumeric, hyphens, underscores, and spaces
										//regexp: /^[a-zA-Z0-9\-_\s]+$/,										
										//similar to: (though, \w supports other Unicode characters)
											regexp: /^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/,
										message: 'Must include valid URL'
									},									
							},
					},
					ytd_sales: {
							validators: {
									notEmpty: {
									 message: 'YTD Sales required'
									},
									stringLength: {
											min: 1,
											max: 10,
									 message: 'no more than 10 digits, including decimal point '
									},
									regexp: {
										//alphanumeric, hyphens, underscores, and spaces
										//regexp: /^[a-zA-Z0-9\-_\s]+$/,										
										//similar to: (though, \w supports other Unicode characters)
											regexp: /^[0-9\.]+$/,
										message: 'can only contain numbers, and decimal point (if used)'
									},									
							},
					},
					notes: {
							validators: {
									
									stringLength: {
											min: 0,
											max: 255,
									 message: 'Notes no more than 255 characters'
									},
														
							},
					},
					
			}
	});
});
</script>

</body>
</html>
