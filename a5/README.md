# LIS 4381 Mobile Web Application

## Jonathan Poteet

### Assignment 5 Requirements:

*5 things*

1. Clone A4 Files
2. Conduct Server Validation
3. Modify files needed for A5
4. Add placeholders.
5. Use PHP to take inputs and use them in a database.

#### README.md file should include the following items:

* Course title, your name, assignment requirements
* Screenshots as per the examples
* Link to local lis4381 web app: http://localhost/repos/lis4381/  

#### Assignment Screenshots:

*Screenshot of Assignment 5 index page*:

![index](images/index.PNG)

Screenshot of Assignment 5 error page*:

![Add petstore](images/petstore2.PNG))

